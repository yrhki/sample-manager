﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using SampleManager.Database.Models;
using SharpCompress.Readers;

namespace SampleManager
{
    static class Helpers
    {
        /// <summary>
        /// Get files by specified file extensions from DirectoryInfo
        /// </summary>
        /// <param name="dir">Directory to search</param>
        /// <param name="searchOption">Depth or search</param>
        /// <param name="extensions">File extensions</param>
        /// <returns></returns>
        public static IEnumerable<FileInfo> GetFilesByExtensions(this DirectoryInfo dir, SearchOption searchOption, params string[] extensions)
        {
            if (extensions == null)
                throw new ArgumentNullException("extensions");
            IEnumerable<FileInfo> files = dir.GetFiles("*.*", searchOption);
            return files.Where(f => extensions.Contains(f.Extension));
        }

        /// <summary>
        /// Convert List of fileinfo to List of Sample
        /// </summary>
        /// <param name="lists">List of sample files</param>
        /// <param name="sampleFolder">Sample pack directory</param>
        /// <returns>Sample packs samples</returns>
        public static List<SampleModel> FileInfoToSample(this List<FileInfo> lists, DirectoryInfo sampleFolder)
        {
            List<SampleModel> samples = new List<SampleModel>();
            lists.ForEach(file =>
            {
                SampleModel addSample = new SampleModel
                {
                    Name = Path.GetFileNameWithoutExtension(file.Name),
                    RelativePath = file.FullName.Remove(file.FullName.IndexOf(sampleFolder.FullName), sampleFolder.FullName.Length + 1),
                    SamplePackID = sampleFolder.Name
                };
                addSample.Init();
                samples.Add(addSample);
            });
            return samples;
        }

        public static void Empty(this DirectoryInfo directory)
        {
            try
            {
                foreach (FileInfo file in directory.GetFiles()) file.Delete();
                foreach (DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Directories or files in use","Error");
            }
        }

        public static List<SampleModel> ArchiveToSamples(FileInfo fileInfo)
        {
            List<SampleModel> samples = new List<SampleModel>();

            using (Stream stream = File.OpenRead(fileInfo.FullName))
            using (IReader reader = ReaderFactory.Open(stream))
            {
                while (reader.MoveToNextEntry())
                    if (!reader.Entry.IsDirectory && SettingsManager.Extensions.Any(ext => reader.Entry.ToString().EndsWith(ext)))
                    {
                        string entry = reader.Entry.ToString();
                        SampleModel addSample = new SampleModel
                        {
                            Name = Path.GetFileNameWithoutExtension(entry),
                            RelativePath = entry,
                            SamplePackID = fileInfo.Name
                        };
                        addSample.Init();
                        samples.Add(addSample);
                    }
            }
            return samples;
        }
    }
    
}

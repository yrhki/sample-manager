﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SampleManager
{
    public static class SettingsManager
    {
        static string[] extensions;

        public static string[] Extensions
        {
            get => Settings.Default.Extensions.Split(';');
            set => Settings.Default.Extensions = string.Join(";", value);
        }

        public static bool VerticalScrollBars { get; set; }

        //internal static List<Models.ScanPath> List_paths { get => list_paths; set => list_paths = value; }

        public static ScrollBarVisibility VerticalScrollBarsVisibility
        {
            get
            {
                if (Settings.Default.VerticalScrollBars)
                    return ScrollBarVisibility.Auto;
                else return ScrollBarVisibility.Hidden;
            }
        }

        /// <summary>
        /// Load settings from settings file
        /// </summary>
        public static void LoadSettings()
        {
            if (Settings.Default.Paths.Contains("||"))
            {
                foreach (string item in Settings.Default.Paths.Split(new string[] { "||" }, StringSplitOptions.None))
                {
                    string[] s = item.Split('|');
                    Holder.ScanPaths.Add(new Models.ScanPath(s[0],bool.Parse(s[1])));
                }
                extensions = Settings.Default.Extensions.Split(';');
            }
            Files.ValidatePaths();
            SaveSettings();
        }

        /// <summary>
        /// Save settings
        /// </summary>
        public static void SaveSettings()
        {

            Settings.Default.Paths = string.Join("||", Holder.ScanPaths);
            Settings.Default.Save();
        }

    }
}

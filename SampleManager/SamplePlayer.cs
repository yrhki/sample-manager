﻿using System;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;

namespace SampleManager
{
    class SamplePlayer
    {
        Slider mediaSlider;
        MediaElement mediaElement;
        Button mediaButton;
        bool isUserDragSlider = false;

        public SamplePlayer(Button control, Slider slider, MediaElement mediaElement)
        {
            mediaButton = control;
            mediaSlider = slider;
            this.mediaElement = mediaElement;
            timer.Tick += Tick;

            mediaButton.Click += MediaButton_Click;
            mediaElement.MediaEnded += MediaElement_Stop;
            mediaSlider.AddHandler(Thumb.DragStartedEvent, new DragStartedEventHandler(MediaSlider_DragStarted));
            mediaSlider.AddHandler(Thumb.DragCompletedEvent, new DragCompletedEventHandler(MediaSlider_DragCompleted));
            mediaSlider.ValueChanged += MediaSlider_ValueChanged;
        }

        private void MediaSlider_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        =>  mediaElement.Position = TimeSpan.FromSeconds(mediaSlider.Value);

        public void PlaySample(Database.Models.SamplePackModel samplePack, Database.Models.SampleModel sample)
        {
            string path = Files.GetSamplePath(samplePack, sample);
            if (path == "") return;

            mediaButton.Content = "Pause";
            mediaElement.Source = new Uri(path);
            mediaElement.Play();
            timer.Start();
        }

        void Media_Pause()
        {
            timer.Stop();
            mediaElement.Pause();
            mediaButton.Content = "Play";
        }

        void Media_Start()
        {
            if (mediaElement.Source == null) return;

            timer.Start();
            mediaElement.Play();
            mediaButton.Content = "Pause";
        }

        void Media_Stop()
        {
            timer.Stop();
            mediaElement.Stop();
            mediaButton.Content = "Play";
            mediaSlider.Value = 0;
        }

        private void MediaButton_Click(object sender, EventArgs e)
        {
            if ((string)mediaButton.Content == "Pause")
                Media_Pause();
            else
                Media_Start();
        }

        private void MediaSlider_DragStarted(object sender, EventArgs e)
        {
            isUserDragSlider = true;
            timer.Stop();
        }

        private void MediaSlider_DragCompleted(object sender, EventArgs e)
        {
            mediaElement.Position = TimeSpan.FromSeconds(mediaSlider.Value);
            isUserDragSlider = false;
            timer.Start();
        }

        private void MediaElement_Stop(object sender, EventArgs e) => Media_Stop();

        DispatcherTimer timer = new DispatcherTimer
        {
            Interval = TimeSpan.FromMilliseconds(80)
        };

        void Tick(object sender, EventArgs e)
        {
            if (mediaElement.Source != null && mediaElement.NaturalDuration.HasTimeSpan && !isUserDragSlider)
            {
                mediaSlider.Minimum = 0;
                mediaSlider.Maximum = mediaElement.NaturalDuration.TimeSpan.TotalSeconds;
                mediaSlider.Value = mediaElement.Position.TotalSeconds;
            }
        }
    }
}

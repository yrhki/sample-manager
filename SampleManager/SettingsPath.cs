﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleManager
{
    [System.Serializable]
    [TypeConverter(typeof(SettingsPathConverter))]
    public class SettingsPath
    {
        public string fullPath;
        public bool isActive;

        public override string ToString()
        {
            return string.Format("{0};{1}",fullPath,isActive);
        }
    }

    public class SettingsPathConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            else
                return base.CanConvertFrom(context, sourceType);
        }

    }

}

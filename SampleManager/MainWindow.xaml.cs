﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using SampleManager.Database.Models;
using MahApps.Metro.Controls;
using System.Windows.Input;
using System.Windows.Data;
using System.Runtime.CompilerServices;

namespace SampleManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        SamplePlayer samplePlayer;
        List<Key> keysDown = new List<Key>();

        public MainWindow()
        {
            InitializeComponent();
            SettingsManager.LoadSettings();

            samplePlayer = new SamplePlayer(Btn_MediaControl, Media_SampleSlider, Media_Sample);

            Holder.Load();

            DataG_Paths.ItemsSource = Holder.ScanPaths;
            DataG_SamplePacks.ItemsSource = Holder.SamplePacks;
            DataG_ListPack.ItemsSource = Holder.ListPacks;
            CBox_AddList.ItemsSource = Holder.ListPacks;
            Tab_Settings.DataContext = Settings.Default;

        }

        private void Click_Search(object sender, RoutedEventArgs e)
        {
            foreach (Models.ScanPath path in Holder.ScanPaths)
            {
                if (path.IsActive)
                    Files.StartSearch(path.DirInfo);
            }
            DataG_SamplePacks.Items.Refresh();
            MessageBox.Show("Done","Search");
        }

        private void SettingsSave(object sender, RoutedEventArgs e)
        {
            SettingsManager.SaveSettings();
        } 

        private void Btn_AddList_Click(object sender, RoutedEventArgs e)
        {
            Holder.AddListPack(new ListPackModel(true));
            DataG_ListPack.Items.Refresh();
            CBox_AddList.Items.Refresh();
        }

        private void Btn_AddListItem_Click(object sender, RoutedEventArgs e)
        {
            Action_AddListItem();
        }

        private void Action_AddListItem()
        {
            ListPackModel listPack = CBox_AddList.SelectedItem as ListPackModel;
            if (Holder.SelectedSample == null || listPack == null) return;
            Holder.AddListPackItem(listPack, new ListPackItemModel(Holder.SelectedSample.RelativePath, Holder.SelectedSamplePack.ID, listPack.ID, true));
            DataG_ListPackItems.Items.Refresh();
            DataG_ListPack.Items.Refresh();
        }


        private void Window_KeyEvent(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.S))
            {
                Holder.Save(Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift));
            }
            if (TabC_Main.SelectedIndex == 0 && TabC_Pack.SelectedIndex == 1 && Keyboard.IsKeyDown(Key.F6))
            {
                Action_AddListItem();
            }
        }

        private void Button_Save(object sender, RoutedEventArgs e)
        {
            Holder.Save(Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Files.ValidatePacks();
        }

        private void Btn_DeleteList_Click(object sender, RoutedEventArgs e)
        {
            if (DataG_ListPack.SelectedIndex == -1) return;

            Holder.DeleteListPack(DataG_ListPack.SelectedItem as ListPackModel);
            DataG_ListPack.Items.Refresh();
        }

        private void RunList(object sender, RoutedEventArgs e)
        {
            if (DataG_ListPack.SelectedIndex == -1) return;
            Files.RunList(DataG_ListPack.SelectedItem as ListPackModel);
        }

        private void Button_ListPackPathControl(object sender, RoutedEventArgs e)
        {
            Button control = sender as Button;

            switch (control.Content)
            {
                case "Add":
                    Holder.AddListPackPath();
                    Combo_PackPaths.SelectedItem = Holder.SelectedListPackPath;
                    Tab_ListDirectory.DataContext = Holder.SelectedListPackPath;
                    break;
                case "Delete":
                    Holder.DeleteListPackPath(Holder.SelectedListPackPath);
                    Combo_PackPaths.SelectedIndex = Item_PathCombo.SelectedIndex = 0;
                    break;
                default:
                    break;
            }
            Combo_PackPaths.Items.Refresh();
            Item_PathCombo.Items.Refresh();
        }



        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid grid = sender as DataGrid;
            if (grid.SelectedIndex == -1) return;

            Type type = grid.SelectedItem.GetType();

            if (type == typeof(ListPackItemModel))
            {
                Tab_Item.DataContext = Holder.SelectedListPackItem = grid.SelectedItem as ListPackItemModel;
                TabC_Lists.SelectedIndex = 2;
                Tab_ItemSample.DataContext = Holder.SelectedListPackItem.Sample;


                Item_PathCombo.SelectedItem = Holder.SelectedListPack.Paths.Find(path => path.ID == Holder.SelectedListPackItem.ListPackDirectoryID);

                if (Keyboard.IsKeyDown(Key.LeftAlt))
                    samplePlayer.PlaySample(Holder.SelectedListPackItem.SamplePack, Holder.SelectedListPackItem.Sample);

            }
            else if (type == typeof(ListPackModel))
            {
                Tab_List.DataContext = Holder.SelectedListPack = grid.SelectedItem as ListPackModel;
                Item_PathCombo.ItemsSource = Holder.SelectedListPack.Paths;
                DataG_ListPackItems.ItemsSource = Holder.SelectedListPack.Items;
                Item_PathCombo.ItemsSource = Holder.SelectedListPack.Paths;
                Combo_PackPaths.ItemsSource = Holder.SelectedListPack.Paths;
                Combo_PackPaths.SelectedIndex = 0;

                TabC_Lists.SelectedIndex = 0;
            }
            else if (type == typeof(SampleModel))
            {
                Tab_Sample.DataContext = Holder.SelectedSample = grid.SelectedItem as SampleModel;
                TabC_Pack.SelectedIndex = 1;
                if (Keyboard.IsKeyDown(Key.LeftAlt))
                {
                    samplePlayer.PlaySample(Holder.SelectedSamplePack, Holder.SelectedSample);
                }
            }
            else if (type == typeof(SamplePackModel))
            {
                Tab_Pack.DataContext = Holder.SelectedSamplePack = grid.SelectedItem as SamplePackModel;
                // Set samples to list
                DataG_Samples.ItemsSource = Holder.SelectedSamplePack.Samples;
                // Change tab page to "Pack"
                TabC_Pack.SelectedIndex = 0;
            }

        }

        /// <summary>
        /// Deleted item from datagrid will be also deleted from database
        /// </summary>
        private new void PreviewKeyDown(object sender, KeyEventArgs e)
        {
            DataGrid grid = sender as DataGrid;
            if (e.Key == Key.Delete && grid.SelectedItems.Count > 0)
            {
                Type type = grid.SelectedItem.GetType();
                object item = grid.SelectedItem;

                if (type == typeof(ListPackItemModel))
                {
                    Holder.DeleteListPackItem(item as ListPackItemModel);
                    Tab_Item.DataContext = null;
                }
                else if (type == typeof(ListPackModel))
                {
                    Holder.DeleteListPack(item as ListPackModel);
                    Tab_List.DataContext = Holder.SelectedListPack = null;
                    DataG_ListPackItems.ItemsSource = null;
                    Tab_Item.DataContext = null;
                }

                grid.Items.Refresh();
            }
        }

        private void Combobox_Selection(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            switch (comboBox.Name)
            {
                case "Combo_PackPaths":
                    Holder.SelectedListPackPath = comboBox.SelectedItem as ListPackDirectoryModel;
                    Tab_ListDirectory.DataContext = Holder.SelectedListPackPath;
                    break;
                default:
                    break;
            }


        }
    }
}

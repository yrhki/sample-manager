﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.WindowsAPICodePack.Dialogs;
using SharpCompress;
using System.Threading.Tasks;
using SharpCompress.Readers;

namespace SampleManager
{
    static class Files
    {
        public static string searchPattern = "*";

        /// <summary>
        /// Select directory
        /// </summary>
        /// <returns>Sekected directory, null on fail</returns>
        public static DirectoryInfo SelectDirectory()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog
            {
                Title = "Select path",
                Multiselect = false,
                IsFolderPicker = true
            };
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                return new DirectoryInfo(dialog.FileName);
            return null;
        }


        /// <summary>
        /// Get fullpath of sample pack by identifying name
        /// </summary>
        /// <param name="id">Identifying name</param>
        /// <returns>Info</returns>
        public static string GetSamplePackPath(string id)
        {
            FileSystemInfo output;
            foreach (Models.ScanPath scanPath in Holder.ScanPaths)
            {
                if (!scanPath.IsActive) continue;

                output = scanPath.DirInfo.GetFileSystemInfos("*",SearchOption.TopDirectoryOnly).FirstOrDefault(info => info.Name == id);
                if (output != null)
                {
                    return output.FullName;
                }
            }
            return null;
        }

        /// <summary>
        /// Checks paths that dont exist in filesystem and removes them
        /// </summary>
        public static void ValidatePaths()
        {
            Holder.ScanPaths.RemoveAll(scanPath => !Directory.Exists(scanPath.FullPath));
        }



        public static void RunList(Database.Models.ListPackModel listPack)
        {

            DirectoryInfo directory = new DirectoryInfo(listPack.Directory);
            directory.Empty();
            if (directory.GetFileSystemInfos().Length > 0) return;

            foreach (Database.Models.ListPackItemModel item in listPack.Items)
            {
                if (!item.IsActive) continue;
                string fileSource = GetSamplePath(item.SamplePack, item.Sample);
                string fileDest = $"{listPack.Directory}{item.FormattedPath}";

                Directory.CreateDirectory(Path.GetDirectoryName(fileDest));

                File.Copy(fileSource,fileDest);

            }
        }

        public static string GetSamplePath(Database.Models.SamplePackModel samplePack, Database.Models.SampleModel sample)
        {
            if (samplePack.FullPath == null || sample.RelativePath == null) return "";
            if (samplePack.Archive)
                if (File.Exists($"{Path.GetTempPath()}\\SampleManager\\Unzip\\{samplePack.ID}\\{sample.RelativePath}"))
                    return $"{Path.GetTempPath()}\\SampleManager\\Unzip\\{samplePack.ID}\\{sample.RelativePath}";
                else
                    return ExtractSample(samplePack, sample);
            return $"{samplePack.FullPath}\\{sample.RelativePath}";
        }


        /// <summary>
        /// Extract sample from pack and return filepath
        /// </summary>
        /// <param name="samplePack"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public static string ExtractSample(Database.Models.SamplePackModel samplePack, Database.Models.SampleModel sample)
        {
            string pathDest = $"{Path.GetTempPath()}\\SampleManager\\Unzip\\{samplePack.ID}\\{sample.RelativePath}";
            bool found = false;
            DirectoryInfo tempDir = Directory.CreateDirectory(Path.GetDirectoryName(pathDest));
            using (Stream stream = File.OpenRead(samplePack.FullPath))
            using (IReader reader = ReaderFactory.Open(stream))
            {
                while (reader.MoveToNextEntry() && !found)
                {
                    if (reader.Entry.ToString() == sample.RelativePath)
                    {
                        reader.WriteEntryToDirectory(tempDir.FullName, new SharpCompress.Common.ExtractionOptions()
                        {
                            Overwrite = true
                        });
                        found = true;
                    }
                }
            }
            return pathDest;
        }

        /// <summary>
        /// Start searching for sample packs on directory
        /// </summary>
        /// <param name="path">Sample pack directory</param>
        public static void StartSearch(DirectoryInfo path)
        {
            foreach (DirectoryInfo sampleFolder in path.GetDirectories())
            {
                List<FileInfo> samples = sampleFolder.GetFilesByExtensions(SearchOption.AllDirectories,SettingsManager.Extensions).ToList();
                Holder.AddSamplePack(new SampleManager.Database.Models.SamplePackModel
                {
                    Name = sampleFolder.Name,
                    FullPath = sampleFolder.FullName,
                    ID = sampleFolder.Name,
                    Samples = samples.FileInfoToSample(sampleFolder),
                    PackFound = true
                });
            }

            foreach (FileInfo archivedSampleFolder in path.GetFilesByExtensions(SearchOption.TopDirectoryOnly,".zip"))
            {
                Holder.AddSamplePack(new SampleManager.Database.Models.SamplePackModel
                {
                    Name = Path.GetFileNameWithoutExtension(archivedSampleFolder.Name),
                    ID = archivedSampleFolder.Name,
                    FullPath = archivedSampleFolder.FullName,
                    Samples = Helpers.ArchiveToSamples(archivedSampleFolder).ToList(),
                    Archive = true,
                    PackFound = true
                });
            }
        }

        public static void ValidatePacks()
        {
            //DirectoryInfo[] directories = Holder.ScanPaths.Select(path => path.DirInfo).ToArray();
            //FileInfo[] files = Holder.ScanPaths.Select(path => path.DirInfo.GetFilesByExtensions(SearchOption.TopDirectoryOnly, ".zip")).Cast<FileInfo>().ToArray();

            foreach (Models.ScanPath path in Holder.ScanPaths)
            {
                string[] dirNames = path.DirInfo.GetDirectories().Select(dir => dir.Name).Cast<string>().Union(
                    path.DirInfo.GetFilesByExtensions(SearchOption.TopDirectoryOnly, ".zip").Select(file => file.Name)).ToArray();

                foreach (Database.Models.SamplePackModel samplePack in Holder.SamplePacks)
                {
                    if (dirNames.Contains(samplePack.ID))
                    {
                        samplePack.FullPath = $"{path.FullPath}\\{samplePack.ID}";
                    }
                }

                //Database.Models.SamplePackModel samplePacks = Holder.SamplePacks.Where(pack => path.DirInfo.GetDirectories().Select(dir => dir.Name).Contains(pack.ID));


            }
        }


    }
}

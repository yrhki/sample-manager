﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleManager.Models
{
    /// <summary>
    /// Directory for scanning sample packs
    /// </summary>
    public class ScanPath
    {
        /// <summary>
        /// Directory info of path
        /// </summary>
        public DirectoryInfo DirInfo { get; set; }
        /// <summary>
        /// Full path to directory
        /// </summary>
        public string FullPath { get { return DirInfo == null ? string.Empty : DirInfo.FullName; } set { Set(); } }
        /// <summary>
        /// Is this directory used next scan
        /// </summary>
        public bool IsActive { get; set; }

        public ScanPath()
        {
            Set();
            IsActive = true;
        }

        public ScanPath(string fullPath, bool isActive)
        {
            DirInfo = new DirectoryInfo(fullPath);
            IsActive = isActive;
        }

        /// <summary>
        /// Set new directory
        /// </summary>
        public void Set() => DirInfo = Files.SelectDirectory();

        /// <summary>
        /// Get text version
        /// Used for saving in settings
        /// </summary>
        /// <returns>FullpPth|IsActive</returns>
        public override string ToString()
        {
            return string.Format("{0}|{1}", FullPath, IsActive);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using Dapper;

namespace SampleManager.Database
{
    public class DatabaseConnection
    {
        public static List<Models.SamplePackModel> LoadSamplePacks()
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                var output = connection.Query<Models.SamplePackModel>("select * from SamplePack", new DynamicParameters());
                return output.ToList();
            }
        }

        public static IEnumerable<Models.ListPackModel> LoadListPacks()
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                var output = connection.Query<Models.ListPackModel>("SELECT * FROM ListPack");
                return output;
            }
        }

        public static IEnumerable<Models.ListPackItemModel> LoadListPackItems(Models.ListPackModel listPack)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                var ouput = connection.Query<Models.ListPackItemModel>("SELECT * FROM ListPackItem WHERE ListPackID = @ID", listPack);
                return ouput;
            }
        }

        public static IEnumerable<Models.ListPackDirectoryModel> LoadListPackPaths(Models.ListPackModel listPack)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                var ouput = connection.Query<Models.ListPackDirectoryModel>("SELECT * FROM ListPackDirectory WHERE ListPackID = @ID", listPack);
                return ouput;
            }
        }

        public static void SaveListPackPath(Models.ListPackDirectoryModel listPackPath)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                if (listPackPath.ID != default(ulong))
                {
                    connection.Execute("UPDATE ListPackDirectory SET Path = @Path, Name = @NAME WHERE ID = @ID", listPackPath);
                }
                else
                {
                    connection.Execute("INSERT INTO ListPackDirectory (Path, Name, ListPackID) VALUES (@Path, @Name, @ListPackID)", listPackPath);

                    ulong id = connection.QueryFirst<ulong>("SELECT ID FROM ListPackDirectory ORDER BY ID DESC LIMIT 1");
                    listPackPath.ID = id;
                }
                listPackPath.UnSaved = false;
            }
        }

        public static IEnumerable<Models.SampleModel> LoadSamples(Models.SamplePackModel samplePack)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                var ouput = connection.Query<Models.SampleModel>("SELECT * FROM Sample WHERE SamplePackID = @ID",samplePack);
                return ouput;
            }
        }

        public static void SaveSamplePack(Models.SamplePackModel samplePack, bool saveSamples = false)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                connection.Execute("REPLACE INTO SamplePack (ID, Name, Label, Artist, Prefix, Archive) VALUES (@ID, @Name, @Label, @Artist, @Prefix, @Archive)", samplePack);
                samplePack.UnSaved = false;

                if (saveSamples)
                {
                    foreach (Models.SampleModel sample in samplePack.Samples.Where(sample => sample.UnSaved))
                    {
                        if (sample.ID != default(uint))
                            connection.Execute("UPDATE Sample SET Name = @Name,BPM = @BPM, Key = @Key,Timing = @Timing,Type = @Type WHERE ID = @ID", sample);
                        else
                        {
                            connection.Execute("INSERT INTO Sample (SamplePackID, RelativePath, Name) VALUES (@SamplePackID, @RelativePath, @Name)", sample);
                            sample.ID = connection.QueryFirst<uint>("SELECT ID FROM Sample ORDER BY ID DESC LIMIT 1");

                        }
                        sample.UnSaved = false;
                    }
                }
            }
        }

        public static void SaveListPack(Models.ListPackModel listPack, bool saveItems)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                if (listPack.ID != default(uint))
                {
                    connection.Execute("UPDATE ListPack SET Directory = @Directory, Name = @Name WHERE ID = @ID", listPack);
                }
                else
                {
                    connection.Execute("INSERT INTO ListPack (Directory, Name) VALUES (@Directory, @Name)", listPack);

                    uint id = connection.QueryFirst<uint>("SELECT ID FROM ListPack ORDER BY ID DESC LIMIT 1");
                    listPack.ID = id;
                }
                listPack.UnSaved = false;

                if (saveItems)
                {
                    foreach (var item in listPack.Items.Where(item => item.UnSaved))
                    {
                        connection.Execute("UPDATE ListPackItem SET Path = @Path, IsActive = @IsActive, ListPackDirID = @ListPackDirectoryID WHERE ID = @ID", item);
                        item.UnSaved = false;
                    }
                    foreach (var item in listPack.Paths.Where(item => item.UnSaved))
                    {
                        connection.Execute("UPDATE ListPackDirectory SET Path = @Path, Name = @NAME WHERE ID = @ID", item);
                        item.UnSaved = false;
                    }
                }
            }
        }

        public static void SaveListPackItem(Models.ListPackItemModel listPackItem)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                if (listPackItem.ID != default(uint))
                {
                    connection.Execute("UPDATE ListPackItem SET Path = @Path, IsActive = @IsActive, ListPackDirID = @ListPackDirectoryID WHERE ID = @ID", listPackItem);
                }
                else
                {
                    connection.Execute("INSERT INTO ListPackItem (Path, ListPackID, SampleID, SamplePackID, IsActive) VALUES (@Path, @ListPackID, @SampleID, @SamplePackID, @IsActive)",listPackItem);

                    uint id = connection.QueryFirst<uint>("SELECT ID FROM ListPackItem ORDER BY ID DESC LIMIT 1");
                    listPackItem.ID = id;
                }
                listPackItem.UnSaved = false;
            }
        }

        public static void DeleteListPack(Models.ListPackModel listPack)
        {
                using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
                {
                    connection.Query<Models.ListPackModel>("DELETE FROM ListPack WHERE ID = @ID", listPack);
                    connection.Execute("DELETE FROM ListPackItem WHERE ListPackID = @ID", listPack);
                    connection.Execute("DELETE FROM ListPackDirectory WHERE ListPackID = @ID", listPack);
                }
        }

        public static void DeleteListPackItem(Models.ListPackItemModel listPackItem)
        {
                using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
                {
                    connection.Query<Models.ListPackItemModel>("DELETE FROM ListPackItem WHERE ID = @ID", listPackItem);
                }
        }

        public static void DeleteListPackPath(Models.ListPackDirectoryModel item)
        {
                using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
                {
                    connection.Query<Models.ListPackDirectoryModel>("DELETE FROM ListPackDirectory WHERE ID = @ID", item);
                }
        }

        public static void SaveSample(Models.SampleModel sample)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                connection.Execute("REPLACE INTO Sample (ID, SamplePackID, RelativePath, Name, BPM, Timing, Key) VALUES (@ID, @SamplePackID, @RelativePath, @Name, @BPM, @Timing, @Key)", sample);
                sample.UnSaved = false;
            }
        }


        private static string ConnectionString(string id = "Default")
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }

    }
}

﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SampleManager.Database.Models
{
    public class Model : INotifyPropertyChanged
    {


        protected bool _unsaved = false;
        public bool UnSaved
        {   get => _unsaved;
            set
            {
                if (Holder.IsLoaded)
                {
                    _unsaved = value;
                    NotifyPropertyChanged("UnSaved");
                }
            }
        }

        public event EventHandler NameChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "", bool changeState = false)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            if (changeState && Holder.IsLoaded)
            {
                UnSaved = true;
                NameChanged?.Invoke(this, new EventArgs());
            }
        }
    }

}

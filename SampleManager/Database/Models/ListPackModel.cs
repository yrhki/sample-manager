﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleManager.Database.Models
{
    public class ListPackModel : Model
    {
        public ulong ID { get; set; }
        public string Directory { get; set; }
        public string Name { get; set; }
        public List<ListPackItemModel> Items { get; set; } = new List<ListPackItemModel>();
        public List<ListPackDirectoryModel> Paths { get; set; } = new List<ListPackDirectoryModel>() { new ListPackDirectoryModel(true)};
        public int Count { get => Items.Count; }

        public ListPackModel(bool isNew = false)
        {
            if (isNew)
            {
                DirectoryInfo directoryInfo = Files.SelectDirectory();
                if (directoryInfo == null)
                {
                    return;
                }
                Directory = directoryInfo.FullName;
                Name = directoryInfo.Name;
            }
        }

        public ListPackModel() { }

    }
}

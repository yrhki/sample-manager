﻿using System.Text.RegularExpressions;

namespace SampleManager.Database.Models
{
    public class SampleModel : Model
    {
        public ulong ID { get; set; }
        public string RelativePath { get; set; }
        public string SamplePackID { get; set; }

        private string _name;
        private string _type;
        private uint _bpm;
        private string _key;
        private string _timing;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyPropertyChanged("Name", true);
            }
        }
        public string Type
        {
            get => _type;
            set
            {
                _type = value;
                NotifyPropertyChanged("Type", true);
            }
        }
        public uint BPM
        {
            get => _bpm;
            set
            {
                _bpm = value;
                NotifyPropertyChanged("Type", true);
            }
        }
        public string Key
        {
            get => _key;
            set
            {
                _key = value;
                NotifyPropertyChanged("Type", true);
            }
        }
        public string Timing
        {
            get => _timing;
            set
            {
                _timing = value;
                NotifyPropertyChanged("Type", true);
            }
        }

        public void Init()
        {
            if (Name == null || Name == string.Empty)
                return;


            Name = Name.Replace('_', ' ');
            Name = Regex.Replace(Name, @"([a-z])([0-Z])", "$1 $2");
        }
    }
}

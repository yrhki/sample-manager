﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleManager;
using SampleManager.Database.Models;
using System.IO;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SampleManager.Database.Models
{
    public class ListPackItemModel : Model
    {
        private string _path;
        private bool _isActive;
        public ulong _listPackDirectoryID;
        public ulong ID { get; set; }
        public ulong ListPackID { get; set; }
        public string SampleID { get; set; }
        public string SamplePackID { get; set; }
        public string Extension { get => System.IO.Path.GetExtension(SampleID); }

        public bool IsActive
        {
            get => _isActive;
            set
            {
                UnSaved = true;
                _isActive = value;
                NotifyPropertyChanged();
            }
        }
        public ulong ListPackDirectoryID
        {
            get => _listPackDirectoryID;
            set
            {
                _listPackDirectoryID = value;
                NotifyPropertyChanged("FormattedPath", true);
            }
        }
        public string Path
        {   get => _path;
            set
            {
                _path = value;
                NotifyPropertyChanged("FormattedPath",true);
            }
        }

        public ListPackDirectoryModel PackPath
        {
            get => ListPackPath;
            set
            {
                if (value == null || value.ID == _listPackDirectoryID) return;
                ListPackDirectoryID = value.ID;
                NotifyPropertyChanged("FormattedPath", true);
            }
        }

        /// <summary>
        /// New item for pack
        /// </summary>
        public ListPackItemModel(string sampleID, string samplePackID, ulong listPackID, bool isActive)
        {
            ListPackID = listPackID;
            SampleID = sampleID;
            SamplePackID = samplePackID;
            IsActive = isActive;
            Path = "%SAMPLE_TYPE%%PACK_PREFIX% %SAMPLE%";
            Init();
        }

        /// <summary>
        /// Item from database
        /// </summary>
        public ListPackItemModel(long id, long listPackID, long listPackDirID, string sampleID, string samplePackID, string path, long isActive)
        {
            ID = (uint)id;
            ListPackID = (ulong)listPackID;
            ListPackDirectoryID = (ulong)listPackDirID;
            Path = path;
            SampleID = sampleID;
            SamplePackID = samplePackID;
            IsActive = isActive == 0 ? false : true;
            Init();
        }


        public void Init()
        {
            Sample.NameChanged += _NameChanged;
            ListPackDirectoryModel.PathsChangeEvent += _NameChanged;
        }

        private void _NameChanged(object sender, EventArgs e)
        {
            NotifyPropertyChanged("FormattedPath");
        }

        public string FormattedPath
        {
            get
            {
                string output = Path;
                SampleModel sample = Sample;
                SamplePackModel samplePack = SamplePack;


                output = output.Replace("%SAMPLE%",sample.Name);
                output = output.Replace("%EXT%", System.IO.Path.GetExtension(SampleID).Remove(0,1));
                output = output.Replace("%SAMPLE_BPM%", sample.BPM.ToString());
                output = output.Replace("%SAMPLE_KEY%", sample.Key);
                output = output.Replace("%SAMPLE_TYPE%", sample.Type);

                output = output.Replace("%PACK_NAME%", samplePack.Name);
                output = output.Replace("%PACK_LABEL%", samplePack.Label);
                output = output.Replace("%PACK_ARTIST%", samplePack.Artist);
                output = output.Replace("%PACK_PREFIX%", samplePack.Prefix);

                return '\\' + ListPackPath?.Path +  output + Extension;
            }
        }

        public ListPackModel ListPack { get => Holder.ListPacks.First(pack => pack.ID == ListPackID); }

        public ListPackDirectoryModel ListPackPath { get => ListPack.Paths.First(path => path.ID == ListPackDirectoryID); }

        public SampleModel Sample { get => SamplePack.Samples.First(sample => sample.RelativePath == SampleID); }

        public SamplePackModel SamplePack { get => Holder.SamplePacks.First(pack => pack.ID == SamplePackID); }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleManager.Database.Models
{
    public class SamplePackModel : Model
    {
        public string ID { get; set; }
        public string FullPath { get; set; }
        public bool Archive { get; set; }
        public bool PackFound { get; set; }

        private string _name;
        private string _label;
        private string _artist;
        private string _prefix;

        public string Name
        {   get => _name;
            set
            {
                _name = value;
                NotifyPropertyChanged("Name",true);
            }
        }
        public string Label
        {
            get => _label;
            set
            {
                _label = value;
                NotifyPropertyChanged("Label", true);
            }
        }
        public string Artist
        {
            get => _artist;
            set
            {
                _artist = value;
                NotifyPropertyChanged("Artist", true);
            }
        }
        public string Prefix
        {
            get => _prefix;
            set
            {
                _prefix = value;
                NotifyPropertyChanged("Prefix", true);
            }
        }

        public List<SampleModel> Samples { get; set; } = new List<SampleModel>();
        public int CountSamples
        {
            get
            {
                return Samples == null ? 0 : Samples.Count;
            }
        }

        public string SampleFullPath(SampleModel sample)
        {
            return $"{FullPath}\\{sample.RelativePath}";
        }

    }
}

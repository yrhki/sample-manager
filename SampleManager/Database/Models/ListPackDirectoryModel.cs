﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleManager.Database.Models
{
    public class ListPackDirectoryModel : Model
    {
        public static event EventHandler PathsChangeEvent;

        private string name;
        private string path;

        public ulong ID { get; set; }
        public ulong ListPackID { get; set; }
        public string Name
        {
            get => name;
            set
            {
                name = value;
                NotifyPropertyChanged("Name", ID != 0);
            }
        }
        public string Path
        {
            get => path;
            set
            {
                path = value;
                NotifyPropertyChanged("Path", ID != 0);
                PathsChangeEvent?.Invoke(this, new EventArgs());
            }
        }
        public bool Edit { get; private set; } = true;


        public ListPackDirectoryModel(string name, string path, ulong listPackID)
        {
            ListPackID = listPackID;
            Name = name;
            Path = path;
        }

        public ListPackDirectoryModel(bool first)
        {
            ListPackID = ID = 0;
            Name = "None";
            Path = string.Empty;
            Edit = false;
        }

        public ListPackDirectoryModel() { }

    }
}

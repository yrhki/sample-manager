﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleManager.Database;
using SampleManager.Database.Models;
using System.IO;
using System.Windows;

namespace SampleManager
{
    class Holder
    {
        public static string Test { get; set; } = "asd";

        public static List<SamplePackModel> SamplePacks { get; set; } = new List<SamplePackModel>();

        public static List<ListPackModel> ListPacks = new List<ListPackModel>();
        public static List<Models.ScanPath> ScanPaths = new List<Models.ScanPath>();

        public static bool IsLoaded { get; private set; }

        public static SamplePackModel SelectedSamplePack { get; set; }
        public static SampleModel SelectedSample { get; set; }
        public static ListPackModel SelectedListPack { get; set; }
        public static ListPackItemModel SelectedListPackItem { get; set; }
        public static ListPackDirectoryModel SelectedListPackPath { get; set; }

        private const string MessageBoxText = "Sample is in list.\nAdd again?";

        public static void Load()
        {
            SamplePacks = DatabaseConnection.LoadSamplePacks();
            ListPacks.AddRange(DatabaseConnection.LoadListPacks());

            foreach (SamplePackModel pack in SamplePacks)
            {
                pack.FullPath = Files.GetSamplePackPath(pack.ID);
                pack.PackFound = pack.FullPath == null ? false : true;
                pack.Samples.AddRange(DatabaseConnection.LoadSamples(pack));
            }

            foreach (ListPackModel listPack in ListPacks)
            {
                listPack.Items.AddRange(DatabaseConnection.LoadListPackItems(listPack));
                listPack.Paths.AddRange(DatabaseConnection.LoadListPackPaths(listPack));
                foreach (var item in listPack.Items)
                {
                    item.Init();
                }
            }
            IsLoaded = true;
        }

        public static void Save(bool all = false)
        {
            if (SelectedSamplePack != null)
                DatabaseConnection.SaveSamplePack(SelectedSamplePack, all);
            if (SelectedListPackItem != null && !all)
                DatabaseConnection.SaveListPackItem(SelectedListPackItem);
            if (SelectedSample != null && !all)
                DatabaseConnection.SaveSample(SelectedSample);
            if (SelectedListPack != null)
                DatabaseConnection.SaveListPack(SelectedListPack, all);
        }


        public static void AddListPack(ListPackModel listPack)
        {
            if (listPack.Directory == null) return;

            DatabaseConnection.SaveListPack(listPack, false);
            ListPacks.Add(listPack);

        }

        public static void DeleteListPack(ListPackModel listPack)
        {
            DatabaseConnection.DeleteListPack(listPack);
            ListPacks.Remove(listPack);
        }

        public static void DeleteListPackItem(ListPackItemModel listPackItem)
        {
            DatabaseConnection.DeleteListPackItem(listPackItem);
            ListPacks.First(list => list.ID == listPackItem.ListPackID).Items.Remove(listPackItem);
        }

        public static void AddListPackItem(ListPackModel listPack, ListPackItemModel listPackItem)
        {
            bool isAdded = listPack.Items.Exists(item => item.SampleID == listPackItem.SampleID);

            if (isAdded)
            {
                if (MessageBox.Show(MessageBoxText, "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                {
                    return;
                }
            }
            listPack.Items.Add(listPackItem);
            DatabaseConnection.SaveListPackItem(listPackItem);
        }

        public static void AddListPackPath()
        {
            ListPackDirectoryModel listPackPath = new ListPackDirectoryModel("New path",null,SelectedListPack.ID);
            SelectedListPack.Paths.Add(listPackPath);
            SelectedListPackPath = SelectedListPack.Paths.Last();
            DatabaseConnection.SaveListPackPath(listPackPath);
        }

        public static void DeleteListPackPath(ListPackDirectoryModel listPackPath)
        {
            SelectedListPack.Paths.Remove(listPackPath);

            foreach (var item in SelectedListPack.Items.Where(item => item.ListPackDirectoryID == listPackPath.ID))
            {
                item.PackPath = SelectedListPack.Paths.First();
                DatabaseConnection.SaveListPackItem(item);
            }
            DatabaseConnection.DeleteListPackPath(listPackPath);
        }

        public static void AddSamplePack(SamplePackModel samplePack)
        {
            SamplePackModel _samplePack = SamplePacks.FirstOrDefault(pack => pack.ID == samplePack.ID);

            if (_samplePack != null)
            {
                //Pack aldready in list
                _samplePack.FullPath = samplePack.FullPath;

                //Check samples that might be removed


                //Check for new samples
                foreach (SampleModel sample in samplePack.Samples)
                {
                    if (!_samplePack.Samples.Any(_sample => _sample.RelativePath == sample.RelativePath))
                    {
                        //New sample
                        Console.WriteLine($"New sample {sample.Name}");
                        _samplePack.Samples.Add(sample);
                        DatabaseConnection.SaveSample(sample);
                    }
                }
            }
            else
            {
                //New pack
                Console.WriteLine($"{samplePack.ID} is added");
                SamplePacks.Add(samplePack);

                //Save pack to database with samples
                DatabaseConnection.SaveSamplePack(samplePack,true);
            }

        }
    }
}

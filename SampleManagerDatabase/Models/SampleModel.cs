﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SampleManagerDatabase.Models
{
    public class SampleModel
    {

        public string RelativePath { get; set; }
        public string Name { get; set; }
        public uint BPM { get; set; }
        public string Key { get; set; }
        public string Timing { get; set; }
        public string SamplePackID { get; set; }

        public string ID
        {
            get
            {
                return $"{SamplePackID}\\{RelativePath}";
            }
        }


        public void Init()
        {
            if (Name == null || Name == string.Empty)
                return;


            Name = Name.Replace('_', ' ');
            Name = Regex.Replace(Name, @"([a-z])([0-Z])", "$1 $2");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleManager;

namespace SampleManagerDatabase.Models
{
    class ListPackItemModel
    {
        public string Path { get; set; }
        public string SampleID { get; set; }
        public string SamplePackID { get; set; }
        public bool IsActive { get; set; }

        public ListPackItemModel(string sampleID, string samplePackID, bool isActive)
        {
            SampleID = sampleID;
            SamplePackID = samplePackID;
            IsActive = isActive;
        }

        public SampleModel Sample
        {
            get
            {
                return Holder.SamplePacks.First();
            }
        }

    }
}

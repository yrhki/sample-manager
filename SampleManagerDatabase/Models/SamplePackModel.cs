﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleManagerDatabase.Models
{
    public class SamplePackModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }
        public string Vendor { get; set; }
        public string Author { get; set; }
        public string Prefix { get; set; }
        public List<SampleModel> Samples { get; set; } = new List<SampleModel>();
        public int CountSamples
        {
            get
            {
                return Samples == null ? 0 : Samples.Count;
            }
        }

        public string SampleFullPath(SampleModel sample)
        {
            return $"{FullPath}\\{sample.RelativePath}";
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using Dapper;

namespace SampleManagerDatabase
{
    public class DatabaseConnection
    {
        public static List<Models.SamplePackModel> LoadSamplePacks()
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                var output = connection.Query<Models.SamplePackModel>("select * from SamplePack", new DynamicParameters());
                return output.ToList();
            }
        }

        public static IEnumerable<Models.SampleModel> LoadSamples(Models.SamplePackModel samplePack)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                var ouput = connection.Query<Models.SampleModel>("SELECT * FROM Sample WHERE SamplePackID = @ID",samplePack);
                return ouput;
            }
        }

        public static void SaveSamplePack(Models.SamplePackModel samplePack)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                connection.Execute("REPLACE INTO SamplePack (ID, Name, Vendor, Author, Prefix) VALUES (@ID, @Name, @Vendor, @Author, @Prefix)", samplePack);
            }
        }

        public static void SaveSample(Models.SampleModel sample)
        {
            using (IDbConnection connection = new SQLiteConnection(ConnectionString()))
            {
                connection.Execute("REPLACE INTO Sample (SamplePackID, RelativePath, Name, BPM, Timing) VALUES (@SamplePackID, @RelativePath, @Name, @BPM, @Timing)", sample);
            }
        }

        private static string ConnectionString(string id = "Default")
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }

    }
}
